<?php
/**
 * Copyright 2011 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage Facebook
 * @author Naitik Shah <naitik@facebook.com> / Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Apache 2.0
 */
namespace Bairwell;

/**
 * Extends the BaseFacebook class with the intent of using
 * PHP sessions to store user ids and access tokens.
 */
class Facebook extends \Bairwell\Facebook\Base
{
    /**
     * Identical to the parent constructor, except that
     * we start a PHP session to store the user ID and
     * access token if during the course of execution
     * we discover them.
     *
     * @see BaseFacebook::__construct in facebook.php
     */
    public function __construct()
    {
        if (!session_id() && !headers_sent()) {
            session_start();
        }
        parent::__construct();
    }

    protected static $kSupportedKeys =
        array('state', 'code', 'access_token', 'user_id');

    /**
     * Provides the implementations of the inherited abstract
     * methods.  The implementation uses PHP sessions to maintain
     * a store for authorization codes, user ids, CSRF states, and
     * access tokens.
     * @param string $key The data name to set
     * @param mixed $value The data value to set
     * @return void
     */
    protected function setPersistentData($key, $value)
    {
        if (in_array($key, self::$kSupportedKeys) === FALSE) {
            self::errorLog('Unsupported key passed to setPersistentData.');
            return;
        }

        $session_var_name = $this->constructSessionVariableName($key);
        $_SESSION[$session_var_name] = $value;
    }

    /**
     * Gets persistent data
     *
     * @param string $key The data name to set
     * @param mixed $default Default data to return if the data does not exist
     * @return mixed
     */
    protected function getPersistentData($key, $default = false)
    {
        if (in_array($key, self::$kSupportedKeys) === FALSE) {
            self::errorLog('Unsupported key passed to getPersistentData.');
            return $default;
        }

        $session_var_name = $this->constructSessionVariableName($key);
        if (isset($_SESSION[$session_var_name]) === TRUE) {
            return $_SESSION[$session_var_name];
        } else {
            return $default;
        }
    }

    /**
     * Clear a persisted data key
     * @param string $key Name of the key
     * @return void
     */
    protected function clearPersistentData($key)
    {
        if (!in_array($key, self::$kSupportedKeys)) {
            self::errorLog('Unsupported key passed to clearPersistentData.');
            return;
        }

        $session_var_name = $this->constructSessionVariableName($key);
        unset($_SESSION[$session_var_name]);
    }

    /**
     * Clears all persisted data
     */
    protected function clearAllPersistentData()
    {
        foreach (self::$kSupportedKeys as $key) {
            $this->clearPersistentData($key);
        }
    }

    /**
     * Constructs a session variable name
     * @param string $key The key name
     * @return string the variable name
     */
    protected function constructSessionVariableName($key)
    {
        $variableName = implode('_', array('fb',
            $this->getAppId(),
            $key));
        return $variableName;
    }
}
