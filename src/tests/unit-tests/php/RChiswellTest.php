<?php
namespace Bairwell\Facebook\Tests;

class RChiswellTest extends \PHPUnit_Framework_TestCase
{
    public function testApiExceptionConstructor_Unknown() {
        $object=new \Bairwell\Facebook\ApiException(Array());
        $this->assertEquals('Unknown Error. Check getResult()',$object->getMessage());
    }

    /**
     * Tests that ApiExpection GetType returns 'Exception' when
     * the error is not a string or an array
     */
    public function testApiExceptionGetType_NotStringOrArray() {
        $object=new \Bairwell\Facebook\ApiException(
            Array(
                'error'=>123
            )
        );
        $this->assertEquals('Exception',$object->getType());
    }

    /**
     * Tests that ApiExpection GetType returns 'Exception' when
     * the error is a array but the type is not set
     */
    public function testApiExceptionGetType_UnsetType() {
        $object=new \Bairwell\Facebook\ApiException(
            Array(
                'error'=>Array('message'=>'test')
            )
        );
        $this->assertEquals('Exception',$object->getType());
    }

    public function testApiExceptionToString() {
        $object=new \Bairwell\Facebook\ApiException(
            Array(

                'error'=>Array('message'=>'test error','type'=>'bad error'),
                'code'=>23
            )
        );
        $this->assertEquals('bad error: test error',$object->__toString());
    }
}