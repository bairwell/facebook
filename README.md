Bairwell_Facebook
=================

**Bairwell_Facebook** is a fork off the official [Facebook PHP SDK](https://github.com/facebook/php-sdk)  available at (in particular version 3.1.1 from commit [4ea94b975f](https://github.com/facebook/php-sdk/commit/4ea94b975f9aee86e5d25c111c94081360a11171).

This fork has the following benefits:

* It is name spaced (Bairwell/Facebook)
* Is Dependency Injector compatible (using [Bairwell/DI](https://bitbucket.org/bairwell/di) )
* Can be installed as a PEAR Module
* Can be autoloaded (using something like [Bairwell/Autoloader](https://bitbucket.org/bairwell/autoloader) or similar
* Has each class in its own file
* All unit tests pass (49 tests pass: 11 out of the original 47 fail on the original code in my test environment)
* Better adherence to the Zend coding standard (still 276 checkstyle errors though - it'll be a long slog fixing those)
* It has slightly better unit test coverage ( 75.2% code coverage vs 73.9% )

Quite a lot of work still needs to be done on this to make it up to the standard of code [Bairwell Web Developers](http://www.bairwell.com) aims for. We may consider an entire rewrite once we've really looked at the code, but this is in the future.

Installation
============
The easiest way to install this module is to install it using the [PEAR Installer](http://pear.bairwell.com).
This installer is the PHP community's de-facto standard for installing PHP components.

    sudo pear channel-discover http://pear.bairwell.com
    sudo pear install --alldeps http://pear.bairwell.com/Bairwell_Facebook

Or to install the dist/Bairwell_Facebook...tgz file
    pear install dist/Bairwell_Facebook*.tgz

As A Dependency On Your Component
---------------------------------

If you are creating a component that relies on Bairwell_Facebook please make sure that you add Bairwell/Facebook to your component's package.xml file:

```xml
<dependencies>
  <required>
    <package>
      <name>Bairwell_Facebook</name>
          <channel>pear.bairwell.com</channel>
          <min>0.0.1</min>
          <max>0.999.999</max>
    </package>
  </required>
</dependencies>
```

Usage
-----
Usage is very very very similar to the official Facebook PHP SDK which has a number of [examples][examples] .

The minimal you'll need to have is:

    $facebook=new \Bairwell\DI::getLibrary('Bairwell\Facebook');
    $facebook->setAppId('YOUR_APP_ID')
               ->setApiSecret('YOUR_APP_SECRET');
    // Get user id
    $user=$facebook->getUser();

to make [API][API] calls:

    if ($user) {
      try {
        // Proceed knowing you have a logged in user who's authenticated.
        $user_profile = $facebook->api('/me');
      } catch (\Bairwell\Facebook\ApiException $e) {
        error_log($e);
        $user = null;
      }
    }

Login or logout url will be needed depending on current user state.

    if ($user) {
      $logoutUrl = $facebook->getLogoutUrl();
    } else {
      $loginUrl = $facebook->getLoginUrl();
    }

The major changes are:
* You do not need to "include" the Facebook code (as long as you have a PR0 Autoloader and/or Dependency Injector)
* The constructor no longer accepts/needs parameters
* The Exception has been changed from "FacebookApiException" in the global namespace to \Bairwell\Facebook\ApiException

[examples]: http://github.com/facebook/php-sdk/blob/master/examples/example.php
[API]: http://developers.facebook.com/docs/api

To build
========
This is a [PHIX](http://phix-project.org) compatible module and if you have PHIX installed you
can just do:

    phing test          <- to run the unit tests
    phing code-review   <- get code quality information
    phing phpdoc        <- get PhpDocumentor docs
    phing pear-package  <- to generate the package

This module has also been built with the [Jenkins CI](http://www.jenkins-ci.org) in mind and
you can use the build.jenkins.xml file to run the tests and update Jenkins.

Development Environment
=======================
If you want to patch or enhance this component, you will need to create a suitable development environment. The easiest way
to do that is to install phix4componentdev:

    # phix4componentdev
    sudo apt-get install php5-xdebug
    sudo apt-get install php5-imagick
    sudo pear channel-discover pear.phix-project.org
    sudo pear -D auto_discover=1 install -Ba phix/phix4componentdev

You can then clone the git repository:

    # ComponentName
    git clone https://bitbucket.org/bairwell/facebook.git

Then, install a local copy of this component's dependencies to complete the development environment:

    # build vendor/ folder
    phing build-vendor

To make life easier for you, common tasks (such as running unit tests, generating code review analytics, and creating the PEAR package) have been automated using [phing](http://phing.info).  You'll find the automated steps inside the build.xml file that ships with the component.

Run the command 'phing' in the component's top-level folder to see the full list of available automated tasks.


License
-------
Except as otherwise noted, the Facebook PHP SDK is licensed under the Apache Licence, Version 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html) .
This code is likewise licensed under the Apache Licence.

The majority of the code Copyright 2011 Facebook, Inc, although some portions are Copyright (c) 2011 [Bairwell Ltd - http://www.bairwell.com](http://www.bairwell.com) .

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations
under the License.

